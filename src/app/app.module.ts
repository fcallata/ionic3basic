import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { MenuPage } from '../pages/menu/menu';
import { ListaUsuarioPage } from '../pages/lista-usuario/lista-usuario';
import { HttpClientModule } from '@angular/common/http';
import { BusquedaPage } from '../pages/busqueda/busqueda';
import { UserDetailPage } from '../pages/user-detail/user-detail';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    MenuPage,
    ListaUsuarioPage,
    BusquedaPage,
    UserDetailPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    MenuPage,
    ListaUsuarioPage,
    BusquedaPage,
    UserDetailPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
