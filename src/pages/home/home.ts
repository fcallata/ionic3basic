import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { MenuPage } from '../menu/menu';
import { ListaUsuarioPage } from '../lista-usuario/lista-usuario';
import { BusquedaPage } from '../busqueda/busqueda';
import { UserDetailPage } from '../user-detail/user-detail';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  nombre: string = "";

  constructor(public navCtrl: NavController, public alertCtrl : AlertController) {

  }
  

  pushBusqueda(){
    this.navCtrl.push(BusquedaPage);
  }

  pushMenu(){
    this.navCtrl.push(MenuPage);
  }

  pushListaUsuario(){
    this.navCtrl.push(ListaUsuarioPage);
  }

  pushDetalle(){
    if(this.nombre == ''){
      let alert = this.alertCtrl.create({
        title: 'Noificación ',
        subTitle: 'El campo no debe estar vacio',
        buttons: ['OK']
      });
      alert.present();
    }else{
      this.navCtrl.push(UserDetailPage, {name: this.nombre});
    }
  }
   
}