import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient} from '@angular/common/http';
import {AlertController} from 'ionic-angular';

import { Observable } from 'rxjs/Observable';

@IonicPage()
@Component({
  selector: 'page-lista-usuario',
  templateUrl: 'lista-usuario.html',
})
export class ListaUsuarioPage {
  data: Observable<any>;
  items: any;
  url: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public httpClient: HttpClient, public alertCtrl : AlertController) {
    this.url = "";    
  }

  ionViewDidLoad() {
    this.loadUser();
  }

  loadUser(){    
    this.data = this.httpClient.get('https://jsonplaceholder.typicode.com/users');
    this.data.subscribe(data => {            
      this.items = data;
    })
  }

  alertList(id){

    let vlist = this.items.filter(function(e,i){ return e.id == id})[0];

    let alert = this.alertCtrl.create({
      title: 'Id: ' + vlist.id,
      subTitle: 'Nombre: ' + vlist.name,
      buttons: ['OK']
    });
    alert.present();
  }
}
