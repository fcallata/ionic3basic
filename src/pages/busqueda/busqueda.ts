import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-busqueda',
  templateUrl: 'busqueda.html',
})
export class BusquedaPage {

  nombre: string = "";
  searchQuery: string = "";
  items: string[];

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {
    this.nombre = navParams.get("name");
  }

  ionViewDidLoad() {
    this.initializeItems();
  }
  initializeItems() {
    this.items = [
      'Jujuy',
      'Salta',
      'Chubut',
      'Cordoba'
    ]
  }

  getItems(ev: any) {

    this.initializeItems();

    const val = ev.target.value;

    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  showalert(){
    let alert = this.alertCtrl.create({
      title: 'new Friend',
      subTitle: 'Your friend, just accept your friend',
      buttons: ['OK']
    });

    alert.present();
  }

}
